DROP DATABASE IF EXISTS messageboard;
CREATE DATABASE messageboard;

\c messageboard;

CREATE TABLE msgs (
  ID SERIAL PRIMARY KEY,
  name VARCHAR,
  text VARCHAR,
  time VARCHAR
);
