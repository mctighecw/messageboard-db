const promise = require('bluebird');

const options = {
  promiseLib: promise
};

const pgp = require('pg-promise')(options);

const connectionString = process.env.DATABASE_URL || 'postgres://localhost:5432/messageboard';
const db = pgp(connectionString);

function getAllMsgs(req, res, next) {
  db.any('SELECT * FROM msgs')
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved all messages'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function createMsg(req, res, next) {
  db.none('INSERT INTO msgs(name, text, time)' +
      'values(${name}, ${text}, ${time})',
    req.body)
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Added a new message'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function removeAllMsgs(req, res, next) {
  db.result('SELECT * FROM msgs; DELETE FROM msgs')
    .then(function (result) {
      res.status(200)
        .json({
          status: 'success',
          message: 'Removed all messages'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

module.exports = {
  getAllMsgs: getAllMsgs,
  createMsg: createMsg,
  removeAllMsgs: removeAllMsgs
};
