const express = require('express');
const router = express.Router();
const db = require('./database/queries');

router.get('/api/messageboard', db.getAllMsgs);
router.post('/api/messageboard', db.createMsg);
router.delete('/api/messageboard', db.removeAllMsgs);

module.exports = router;
