import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import { request, requestUrl } from '../utils/network';
import styles from '../styles/styles';

class NewMessage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      addDialog: false,
      newMessageName: '',
      newMessageText: ''
    };
  }

  handleAddNewMessage = () => {
    this.saveData()
      .then(() => {
        this.props.getMessages();
      })
      .then(() => {
        this.handleClear();
      });
  }

  saveData = () => {
    const promise = new Promise((resolve, reject) => {
      const { newMessageName, newMessageText } = this.state;
      const { messages } = this.props;
      const timestamp = new Date().toJSON();

      const url = `${requestUrl}`;
      request.post(url)
        .send({ name: newMessageName, text: newMessageText, time: timestamp })
        .then((res) => {
          resolve();
        })
        .catch((err) => {
          console.log(err)
          reject();
        });
    });
    return promise;
  }

  handleClear = () => {
    this.setState({
      addDialog: false,
      newMessageName: '',
      newMessageText: ''
    });
  }

  render() {
    const { addDialog, newMessageName, newMessageText } = this.state;

    return (
      <div>
        <Dialog
          title="ADD MESSAGE"
          actions={[
            <FlatButton
              label="ADD"
              disabled={newMessageName === '' || newMessageText === ''}
              onClick={this.handleAddNewMessage}
            />,
            <FlatButton
              label="CANCEL"
              onClick={this.handleClear}
            />
          ]}
          modal={true}
          open={addDialog}
          onRequestClose={this.handleClear}
        >
          <TextField
            hintText="Name"
            hintStyle={styles.textFieldHint}
            onChange={(e, value) => { this.setState({ newMessageName: value })}}
          />
          <br />
          <TextField
            hintText="New message"
            multiLine={true}
            style={styles.textFieldWidth}
            hintStyle={styles.textFieldHint}
            onChange={(e, value) => { this.setState({ newMessageText: value })}}
          />
        </Dialog>
      </div>
    );
  }
}

export default NewMessage;
