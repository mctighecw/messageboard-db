import React from 'react';
import styles from '../styles/styles';
import { formatDate } from '../utils/functions';

const Messages = ({ messages }) => {
  return (
    <div style={styles.messageContainer}>
      {messages !== null && messages.length > 0 ?
        (messages.slice(0).reverse().map( (message, i) => {
          const timestamp = formatDate(message.time);

            return (
              <div key={i}>
                <div style= {styles.msgMain}>
                  <span style={styles.msgName}>{message.name}</span>
                  <span style={styles.msgDate}>{timestamp}</span>
                </div>
                <div style={styles.clearFloat} />
                <div style={styles.msg}>
                  {message.text}
                </div>
                {messages.length - 1 !== i ? <div style={styles.msgDivider} /> : null}
              </div>
            );
          })
        ) : <div style={styles.noMessages}>No messages.</div>
      }
    </div>
  );
}

export default Messages;
