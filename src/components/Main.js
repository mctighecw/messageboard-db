import React from 'react';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import NewMessage from './NewMessage';
import Messages from './Messages';
import ConfirmDialog from './ConfirmDialog';
import styles from '../styles/styles';
import { formatToday } from '../utils/functions';
import { request, requestUrl } from '../utils/network';

import {
  WHITE,
  DARKGREY
} from '../styles/constants';

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: null
    };
  }

  getMessages = () => {
    const promise = new Promise((resolve, reject) => {
      const url = `${requestUrl}`;

      request.get(url)
        .then((res) => {
          const text = JSON.parse(res.text);
          resolve(
            this.setState({ messages: text.data })
          );
        });
    });
    return promise;
  }

  handleClickConfirm = () => {
    const { messages } = this.state;

    if (messages.length > 0) {
      this.confirmDialog.setState({ confirmDialogOpen: true });
    }
  }

  clearMessages = () => {
    const url = `${requestUrl}`;
    request.delete(url)
      .then((res) => {
        this.getMessages();
      });
  }

  componentDidMount = () => {
    this.getMessages();
  }

  render() {
    const { messages } = this.state;
    const today = formatToday(new Date());

    return (
      <div style={styles.main}>
        <AppBar
          title="Messageboard"
          titleStyle={styles.appBarTitle}
          iconStyleLeft={styles.appBarLeft}
          iconStyleRight={styles.appBarDate}
          iconElementRight={<div>{today}</div>}
        />

        <div style={styles.buttonBox}>
          <RaisedButton
            label="Add New"
            backgroundColor={DARKGREY}
            labelColor={WHITE}
            style={styles.addButton}
            onTouchTap={() => { this.newMessageDialog.setState({ addDialog: true }); }}
          />
          <RaisedButton
            label="Clear All"
            backgroundColor={DARKGREY}
            labelColor={WHITE}
            style={styles.clearButton}
            onTouchTap={this.handleClickConfirm}
          />
        </div>

        <NewMessage
          ref={element => { this.newMessageDialog = element; }}
          messages={messages}
          getMessages={this.getMessages}
        />
        <ConfirmDialog
          ref={element => { this.confirmDialog = element; }}
          messages={messages}
          clearMessages={this.clearMessages}
        />

        <Messages messages={messages} />

        <div style={styles.footerBox}>
          <div style={styles.footerContent}>
            &copy; 2018, Christian McTighe. Coded by Hand.
          </div>
        </div>
      </div>
    );
  }
}

export default Main;
