import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import styles from '../styles/styles';

class ConfirmDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      confirmDialogOpen: false
    };
  }

  handleClearConfirm = () => {
    this.setState({ confirmDialogOpen: false });
    this.props.clearMessages();
  }

  render() {
    const { confirmDialogOpen } = this.state;

    return (
      <div>
        <Dialog
          title="DELETE MESSAGES"
          actions={[
            <FlatButton
              label="DELETE ALL"
              onClick={this.handleClearConfirm}
            />,
            <FlatButton
              label="CANCEL"
              onClick={() => { this.setState({ confirmDialogOpen: false }); }}
            />
          ]}
          modal={true}
          open={confirmDialogOpen}
          onRequestClose={() => { this.setState({ confirmDialogOpen: false }); }}
        >
          <div style={styles.confirmClearText}>Are you sure that you want to delete all of the messages?</div>
        </Dialog>
      </div>
    );
  }
}

export default ConfirmDialog;
