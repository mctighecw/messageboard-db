import React from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import customMuiTheme from './styles/customMuiTheme';
import WebFont from 'webfontloader';
import Routes from './Routes';

injectTapEventPlugin();

WebFont.load({
  google: {
    families: ['Source Sans Pro']
  }
});

class App extends React.Component {
  render() {
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(customMuiTheme)}>
        <Routes />
      </MuiThemeProvider>
    );
  }
}

export default App;
