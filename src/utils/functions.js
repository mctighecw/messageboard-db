export const formatDate = (timestamp) => {
  const dateFormat = require('dateformat');
  return dateFormat(timestamp, 'h:MM TT, dS mmmm yyyy');
}

export const formatToday = (timestamp) => {
  const dateFormat = require('dateformat');
  return dateFormat(timestamp, 'dS mmmm yyyy');
}
