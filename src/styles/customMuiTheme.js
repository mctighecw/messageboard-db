import {
  FONTPRIMARY,
  WHITE,
  BLACK,
  LIGHTGREY,
  MEDIUMGREY,
  DARKGREY
} from './constants';

import { fade } from 'material-ui/utils/colorManipulator';

export default {
  fontFamily: FONTPRIMARY,
  palette: {
    primary1Color: DARKGREY,
    primary2Color: DARKGREY,
    primary3Color: MEDIUMGREY,
    accent1Color: MEDIUMGREY,
    accent2Color: MEDIUMGREY,
    accent3Color: MEDIUMGREY,
    textColor: DARKGREY,
    alternateTextColor: DARKGREY,
    canvasColor: WHITE,
    borderColor: LIGHTGREY,
    disabledColor: fade(LIGHTGREY, 0.5),
    pickerHeaderColor: LIGHTGREY,
    clockCircleColor: fade(LIGHTGREY, 0.4),
    shadowColor: BLACK
  }
};
