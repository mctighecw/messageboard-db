export const FONTPRIMARY = 'Source Sans Pro, sans-serif';
export const WHITE = '#FFF';
export const BLACK = '#000';
export const LIGHTGREY = '#DCDCDC';
export const MEDIUMGREY = '#848484';
export const DARKGREY = '#373738';
