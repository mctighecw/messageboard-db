import {
  FONTPRIMARY,
  WHITE,
  BLACK,
  LIGHTGREY,
  MEDIUMGREY,
  DARKGREY
} from './constants';

const styles = {
  main: {
    fontFamily: FONTPRIMARY,
    overflowY: 'scroll'
  },
  appBarTitle: {
    color: WHITE
  },
  appBarLeft: {
    display: 'none'
  },
  appBarDate: {
    color: WHITE,
    display: 'flex',
    alignSelf: 'center'
  },
  buttonBox: {
    display: 'flex',
    justifyContent: 'center',
    margin: '30px 0'
  },
  addButton: {
    width: 175,
    height: 50,
    marginRight: 10
  },
  clearButton: {
    width: 175,
    height: 50
  },
  textFieldWidth: {
    width: '100%'
  },
  textFieldHint: {
    color: MEDIUMGREY
  },
  messageContainer: {
    maxWidth: '80%',
    border: `1px solid ${LIGHTGREY}`,
    borderRadius: 5,
    padding: '20px 20px 0 20px',
    margin: '20px auto 60px auto'
  },
  msgMain: {
    fontSize: 16,
    lineHeight: 1.6,
    paddingTop: 5
  },
  noMessages: {
    fontSize: 18,
    lineHeight: 1.4,
    paddingBottom: 20
  },
  msg: {
    color: DARKGREY,
    fontWeight: 200,
    fontSize: 16,
    lineHeight: 1.3,
    whiteSpace: 'pre-line',
    paddingTop: 5,
    paddingBottom: 15,
    marginBottom: 5
  },
  msgName: {
    float: 'left',
    fontWeight: 600,
    color: BLACK
  },
  msgDate: {
    float: 'right'
  },
  msgDivider: {
    borderBottom: `1px solid ${LIGHTGREY}`,
  },
  clearFloat: {
    clear: 'both'
  },
  confirmClearText: {
    fontSize: 16,
    color: BLACK
  },
  footerBox: {
    position: 'fixed',
    bottom: 0,
    left: 0,
    height: 40,
    width: '100%',
    backgroundColor: DARKGREY
  },
  footerContent: {
    color: WHITE,
    padding: 10,
    marginTop: 2
  }
};

export default styles;
