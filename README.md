# README

This is a small React app with a PostgreSQL database for posting short messages to a message board.

## App Information

App Name: messageboard-db

Created: February-March 2018

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/messageboard-db)

## Notes

### Frontend

* React
* React Router
* Material UI
* Google Fonts
* Webpack
* ES6
* Promises
* SuperAgent

### Backend

* Node
* PostgreSQL

Last updated: 2024-11-21
